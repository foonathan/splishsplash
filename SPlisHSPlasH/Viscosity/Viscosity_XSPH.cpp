#include "Viscosity_XSPH.h"
#include "SPlisHSPlasH/TimeManager.h"
#include "../Simulation.h"

using namespace SPH;

Viscosity_XSPH::Viscosity_XSPH(FluidModel *model) :
	ViscosityBase(model)
{
}

Viscosity_XSPH::~Viscosity_XSPH(void)
{
}

void Viscosity_XSPH::step()
{
	Simulation *sim = Simulation::getCurrent();
	const unsigned int nFluids = sim->numberOfFluidModels();
	const unsigned int numParticles = m_model->numActiveParticles();

	const Real h = TimeManager::getCurrent()->getTimeStepSize();
	const Real invH = (static_cast<Real>(1.0) / h);

	// Compute viscosity forces (XSPH)
	#pragma omp parallel default(shared)
	{
		#pragma omp for schedule(static)  
		for (int i = 0; i < (int)numParticles; i++)
		{
			const Vector3r &xi = m_model->getPosition(i);
			const Vector3r &vi = m_model->getVelocity(i);
			Vector3r &ai = m_model->getAcceleration(i);
			const Real density_i = m_model->getDensity(i);

			//////////////////////////////////////////////////////////////////////////
			// Fluid
			//////////////////////////////////////////////////////////////////////////
			auto modelPID = m_model->getPointSetIndex();
			for (unsigned int id = 0; id < nFluids; id++)
			{
				FluidModel *fm_neighbor = sim->getFluidModel(id);
				for (unsigned int j = 0; j < sim->numberOfNeighbors(modelPID, fm_neighbor->getPointSetIndex(), i); j++)
				{
					const unsigned int neighborIndex = sim->getNeighbor(modelPID, fm_neighbor->getPointSetIndex(), i, j);
					const Vector3r &xj = fm_neighbor->getPosition(neighborIndex);
					const Vector3r &vj = fm_neighbor->getVelocity(neighborIndex);

					// Viscosity
					const Real density_j = fm_neighbor->getDensity(neighborIndex);
					ai -= invH * m_viscosity * (fm_neighbor->getMass(neighborIndex) / density_j) * (vi - vj) * sim->W(xi - xj);
				}
			}
		}
	}
}


void Viscosity_XSPH::reset()
{
}

