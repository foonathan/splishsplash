#ifndef __Density_Evolve_h__
#define __Density_Evolve_h__

#include "DensityBase.h"

namespace SPH
{
	/** \brief Computes the density by numerically evolving it using the continuity equation.
	 */
	class DensityComputation_Evolve : public DensityComputationBase
	{
	public:
		/** Computes the density of the specified particle.
		 */
		static Real computeDensityOf(FluidModel* model, int i);

		void computeDensities(unsigned int fluidModelIndex) override;
		void reset() override;

		bool clampLowDensities() const override
		{
			return false;
		}
	};

	/** \brief Computes the density by numerically evolving it using the continuity equation at the surface only.
	 */
	class DensityComputation_EvolveSurface : public DensityComputationBase
	{
	public:
		void computeDensities(unsigned int fluidModelIndex) override;
		void reset() override;

		bool clampLowDensities() const override
		{
			return false;
		}
	};
}

#endif
