#include "Density_Ghost.h"
#include "SPlisHSPlasH/TimeManager.h"
#include "SPlisHSPlasH/Simulation.h"

using namespace SPH;

void DensityComputation_Ghost::computeDensities(const unsigned int fluidModelIndex)
{
	auto sim = Simulation::getCurrent();
	auto model = sim->getFluidModel(fluidModelIndex);

	// update ghost particle position
	update(fluidModelIndex);

	// compute densities
	#pragma omp parallel for
	for (int i = 0; i < (int) model->numActiveParticles(); i++)
	{
		auto xi = model->getPosition(i);

		// normal density
		auto density = DensityComputation_Standard::computeDensityOf(model, i);

		// and add ghost density
		forall_ghost_neighbors(
			density += model->getDensity0() * ghost_volume * sim->W(xi - xj);
		)

		model->setDensity(i, density);
	}
}

void DensityComputation_Ghost::reset()
{
	auto sim = Simulation::getCurrent();

	last_sample_count = 0;

	// compute sampling radius
	sampling_radius = 2 * sim->getParticleRadius();
	sampling_radius2 = sampling_radius * sampling_radius;
	cell_size = sampling_radius / std::sqrt(3);

	// block static cells
	blocked_cells.clear();
	for (unsigned int id = 0; id < sim->numberOfBoundaryModels(); ++id)
	{
		auto model = sim->getBoundaryModel(id);
		if (model->getRigidBodyObject()->isDynamic())
			continue;

		for (unsigned int i = 0; i < model->numberOfParticles(); ++i)
			block_cell(model->getPosition(i));
	}

	// do initial sample
	for (unsigned int id = 0; id < sim->numberOfFluidModels(); ++id)
		resample(id);

	// need to do a new neighborhood search
	Simulation::getCurrent()->performNeighborhoodSearch();

	for (unsigned int id = 0; id < sim->numberOfFluidModels(); ++id)
	{
		// the mass is set to 80%, we need to correct that
		resetMasses(id);
		// also recompute the densities so they are immediately available
		computeDensities(id);
	}
}

void DensityComputation_Ghost::update(unsigned int fluidModelIndex)
{
	constexpr auto sample_step = 8u;

	// update ghost particles
	if (last_sample_count % sample_step == 0)
		resample(fluidModelIndex);
	else
		integrate(fluidModelIndex);
	++last_sample_count;

	// need to do a new neighborhood search
	Simulation::getCurrent()->performNeighborhoodSearch();
}

void DensityComputation_Ghost::resample(unsigned int fluidModelIndex)
{
	Simulation* sim = Simulation::getCurrent();
	FluidModel* fm = sim->getFluidModel(fluidModelIndex);

	auto static_blocks = blocked_cells;

	// block dynamic particles
	for (unsigned int id = 0; id < sim->numberOfBoundaryModels(); ++id)
	{
		auto model = sim->getBoundaryModel(id);
		if (!model->getRigidBodyObject()->isDynamic())
			continue;

		for (unsigned int i = 0; i < model->numberOfParticles(); ++i)
			block_cell(model->getPosition(i));
	}
	for (unsigned int id = 0; id < sim->numberOfFluidModels(); ++id)
	{
		auto model = sim->getFluidModel(id);
		for (unsigned int i = 0; i < model->numActiveParticles(); ++i)
				block_cell(model->getPosition(i));
	}

	// determine seeds
	std::vector<ActiveCell> active_list;
	for (unsigned int i = 0; i < fm->numActiveParticles(); ++i)
	{
		if (isFreeSurfaceParticle(fluidModelIndex, i))
			active_list.emplace_back(fm->getPosition(i), fm->getVelocity(i));
	}

	// sample
	resamplePoissonDisk(fluidModelIndex, std::move(active_list));

	// reset block to static blocks
	blocked_cells = std::move(static_blocks);
}

void DensityComputation_Ghost::resamplePoissonDisk(unsigned int fluidModelIndex, std::vector<ActiveCell>&& active_list)
{
	Simulation* sim = Simulation::getCurrent();
	GhostModel* gm = sim->getGhostModel(fluidModelIndex);

	const auto max_tries = 8u;
	const auto max_depth = static_cast<unsigned>(1.2 * sim->getSupportRadius() / sampling_radius);

	auto coord_dist = std::uniform_real_distribution<Real>(-2 * sampling_radius, +2 * sampling_radius);

	std::vector<Vector3r> ghost_positions, ghost_velocities;
	while (!active_list.empty())
	{
		auto  index = std::uniform_int_distribution<std::size_t>(0, active_list.size() - 1)(engine);
		auto& cur_seed = active_list[index];

		auto found = false;
		for (int cur_trie = 0; cur_trie < int(max_tries); ++cur_trie)
		{
			auto x = coord_dist(engine);
			auto y = coord_dist(engine);
			auto z = coord_dist(engine);

			auto length2 = x * x + y * y + z * z;
			if (length2 <  sampling_radius2 || length2 > 4 * sampling_radius2)
			{
				--cur_trie;
				continue;
			}

			Vector3r point = Vector3r(x, y, z) + cur_seed.position;
			auto cell_pos  = cell_pos_of(point);

			auto is_occupied_cell = [&]
			{
				auto iter = blocked_cells.find(cell_pos);
				if (iter == blocked_cells.end())
					return false;

				auto& cell = iter->second;
				if (cell.dest_index < 0)
					// not a ghost particle, blocked
					return true;
				else if (cell.depth <= cur_seed.depth)
					// ghost particle with higher priority, blocked
					return true;
				else
					// otherwise can override the cell
					return false;
			}();
			if (is_occupied_cell)
				continue;

			auto is_too_close = [&]
			{
				for (int dz = -1; dz <= 1; ++dz)
					for (int dy = -1; dy <= 1; ++dy)
						for (int dx = -1; dx <= 1; ++dx)
						{
							if (dx == 0 && dy == 0 && dz == 0)
								continue;

							CellPos cur_pos = cell_pos;
							cur_pos.x() += dx;
							cur_pos.y() += dy;
							cur_pos.z() += dz;

							auto iter = blocked_cells.find(cur_pos);
							if (iter == blocked_cells.end())
								continue;

							auto& cell = iter->second;
							if ((cell.position - point).squaredNorm() < sampling_radius2)
								return true;
						}

				return false;
			}();
			if(is_too_close)
				continue;

			found = true;

			auto iter = blocked_cells.find(cell_pos);
			if (iter == blocked_cells.end())
			{
				// insert new ghost particle
				blocked_cells.emplace(cell_pos, BlockedCell{point, int(ghost_positions.size()), cur_seed.depth + 1});
				ghost_positions.push_back(point);
				ghost_velocities.push_back(cur_seed.velocity);
			}
			else
			{
				// replace existing ghost particle
				auto& cell = iter->second;
				cell.position = point;
				cell.depth = cur_seed.depth + 1;

				ghost_positions[cell.dest_index]  = point;
				ghost_velocities[cell.dest_index] = cur_seed.velocity;
			}

			if (cur_seed.depth < max_depth)
				active_list.push_back(cur_seed.seed_another(point));
		}

		if (!found)
			active_list.erase(active_list.begin() + index);
	}

	gm->resize(ghost_positions.size(), ghost_positions.data(), ghost_velocities.data());
	ghost_volume = 4. / 3 * EIGEN_PI * sim->getParticleRadius() * sim->getParticleRadius() * sim->getParticleRadius();
}

void DensityComputation_Ghost::integrate(unsigned int fluidModelIndex)
{
	auto sim = Simulation::getCurrent();
	auto model = sim->getGhostModel(fluidModelIndex);
	const Real h = TimeManager::getCurrent()->getTimeStepSize();

	// seed velocities from the fluid
	#pragma omp parallel for
	for (int i = 0; i < (int) model->numberOfParticles(); i++)
	{
		auto min_dist = 0;
		auto xi = model->getPosition(i);
		forall_fluid_neighbors(
			auto dist = (xi - xj).squaredNorm();
			if (min_dist == 0 || dist < min_dist)
			{
				model->setVelocity(i, fm_neighbor->getVelocity(neighborIndex));
				dist = min_dist;
			}
		)
		if (min_dist == 0)
			// no neighbors, set velocity to zero
			model->setVelocity(i, Vector3r(0, 0, 0));
	}

	// integrate
	#pragma omp parallel for
	for (int i = 0; i < (int) model->numberOfParticles(); i++)
		model->setPosition(i, model->getPosition(i) + h * model->getVelocity(i));
}
