#include "DensityBase.h"
#include "SPlisHSPlasH/Simulation.h"

using namespace SPH;

void DensityComputationBase::resetMasses(unsigned int fluidModelIndex)
{
	auto sim = Simulation::getCurrent();
	auto model = sim->getFluidModel(fluidModelIndex);

	auto volume = std::pow(2 * sim->getParticleRadius(), 3);
	model->setVolume(volume);

	auto mass = model->getDensity0() * volume;
	#pragma omp parallel for
	for (int i = 0; i < model->numActiveParticles(); ++i)
		model->setMass(i, mass);
}

bool DensityComputationBase::isFreeSurfaceParticle(unsigned int fluidModelIndex, unsigned int i)
{
	auto sim   = Simulation::getCurrent();
	auto model = sim->getFluidModel(fluidModelIndex);

	auto xi = model->getPosition(i);

	// compute surface normal using the gradient of the color field
	Vector3r normal = Vector3r::Zero();
	forall_fluid_neighbors(
		normal += fm_neighbor->getVolume(neighborIndex) * sim->gradW(xi - xj);
	)
	forall_boundary_neighbors(
		normal += bm_neighbor->getVolume(neighborIndex) * sim->gradW(xi - xj);
	)
	auto length2 = normal.squaredNorm();

	constexpr auto surface_threshhold2 = 3 * 3;
	auto is = length2 > surface_threshhold2;
	model->setFreeSurface(i, is);
	return is;
}

Real DensityComputation_Standard::computeDensityOf(FluidModel *model, int i)
{
	auto sim = Simulation::getCurrent();

	// Compute current density for particle i
	Real density = model->getVolume(i) * sim->W_zero();
	const Vector3r &xi = model->getPosition(i);

	//////////////////////////////////////////////////////////////////////////
	// Fluid
	//////////////////////////////////////////////////////////////////////////
	forall_fluid_neighbors(
		density += fm_neighbor->getVolume(neighborIndex) * sim->W(xi - xj);
	)

	//////////////////////////////////////////////////////////////////////////
	// Boundary
	//////////////////////////////////////////////////////////////////////////
	forall_boundary_neighbors(
		// Boundary: Akinci2012
		density += bm_neighbor->getVolume(neighborIndex) * sim->W(xi - xj);
	)

	density *= model->getDensity0();
	return density;
}

void DensityComputation_Standard::computeDensities(const unsigned int fluidModelIndex)
{
	Simulation *sim = Simulation::getCurrent();
	FluidModel *model = sim->getFluidModel(fluidModelIndex);

	#pragma omp parallel for
	for (int i = 0; i < (int)model->numActiveParticles(); i++)
	{
		model->setDensity(i, computeDensityOf(model, i));
	}
}

