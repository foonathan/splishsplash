#include "Density_Evolve.h"
#include "SPlisHSPlasH/Simulation.h"
#include "SPlisHSPlasH/TimeManager.h"

using namespace SPH;

Real DensityComputation_Evolve::computeDensityOf(FluidModel *model, int i)
{
	const auto sim = Simulation::getCurrent();
	const auto h = TimeManager::getCurrent()->getTimeStepSize();
	const auto density0 = model->getDensity0();

	auto density = model->getDensity(i);
	if (density <= 0)
		// it is a new particle, the new density is the rest density
		return density0;

	const auto xi = model->getPosition(i);
	const auto vi = model->getVelocity(i);

	Real dot_density = 0;
	auto is_splash = isFreeSurfaceParticle(model->getModelIndex(), i);
	forall_fluid_neighbors(
		const auto  vj = fm_neighbor->getVelocity(neighborIndex);

		const auto volume = fm_neighbor->getVolume(neighborIndex);
		dot_density += volume * (vi - vj).dot(sim->gradW(xi - xj));

		if (is_splash && !isFreeSurfaceParticle(fm_neighbor->getModelIndex(), neighborIndex))
			is_splash = false;
	)
	forall_boundary_neighbors(
		const auto  vj = bm_neighbor->getVelocity(neighborIndex);

		const auto volume = bm_neighbor->getVolume(neighborIndex);
		dot_density += volume * (vi - vj).dot(sim->gradW(xi - xj));

		is_splash = false;
	)
	dot_density *= density0;

	if (is_splash)
		// reset density of a splash back to rest density
		return density0;
	else
		return density + dot_density * h;
}

void DensityComputation_Evolve::computeDensities(unsigned int fluidModelIndex)
{
	auto sim = Simulation::getCurrent();
	auto model = sim->getFluidModel(fluidModelIndex);

	#pragma omp parallel for
	for (int i = 0; i < model->numActiveParticles(); ++i)
		model->setDensity(i, computeDensityOf(model, i));
}

void DensityComputation_Evolve::reset()
{
	auto sim = Simulation::getCurrent();

	// reset density of all particles to rest density
	#pragma omp parallel for
	for (auto id = 0u; id < sim->numberOfFluidModels(); ++id)
	{
		auto model = sim->getFluidModel(id);
		auto density0 = model->getDensity0();

		#pragma omp parallel for
		for (int i = 0; i < model->numActiveParticles(); ++i)
			model->setDensity(i, density0);
	}
}

void DensityComputation_EvolveSurface::computeDensities(unsigned int fluidModelIndex)
{
	auto sim = Simulation::getCurrent();
	auto model = sim->getFluidModel(fluidModelIndex);

	#pragma omp parallel for
	for (int i = 0; i < model->numActiveParticles(); ++i)
	{
		auto xi = model->getPosition(i);

		Real total_weight   = sim->W_zero();
		Real surface_weight = isFreeSurfaceParticle(fluidModelIndex, i) * sim->W_zero();
		forall_fluid_neighbors(
			auto weight = sim->W(xi - xj);

			total_weight   += weight;
			surface_weight += isFreeSurfaceParticle(fm_neighbor->getModelIndex(), neighborIndex) * weight;
		)
		auto factor = Real(surface_weight) / total_weight;

		auto evolve = DensityComputation_Evolve::computeDensityOf(model, i);
		auto standard = DensityComputation_Standard::computeDensityOf(model, i);
		model->setDensity(i, factor * evolve + (1 - factor) * standard);
	}
}

void DensityComputation_EvolveSurface::reset()
{
	auto sim = Simulation::getCurrent();

	// need to do the reset of DensityComputation_Evolve
	DensityComputation_Evolve().reset();

	// because clamping is disabled, the masses must not be reduced
	#pragma omp parallel for
	for (auto id = 0u; id < sim->numberOfFluidModels(); ++id)
		resetMasses(id);
}
