#include "Density_EvolveHybrid.h"
#include "SPlisHSPlasH/Simulation.h"
#include "SPlisHSPlasH/TimeManager.h"

using namespace SPH;

void DensityComputation_EvolveHybrid::computeDensities(unsigned int fluidModelIndex)
{
	constexpr unsigned int sample_step = 128;

	const auto sim = Simulation::getCurrent();
	const auto model = sim->getFluidModel(fluidModelIndex);
	const auto max_density = 1.2 * model->getDensity0();

	if (last_sample_count % sample_step == 0)
	{
		// sample new ghost particles
		ghost.resample(fluidModelIndex);

		// need to do a new neighborhood search, as ghost particles changed
		sim->performNeighborhoodSearch();

		// use them to get the density
		ghost.computeDensities(fluidModelIndex);

		#pragma omp parallel for
		for (int i = 0; i < model->numActiveParticles(); ++i)
		{
			auto& density = model->getDensity(i);
			if (density > max_density)
				density = max_density;
		}
	}
	++last_sample_count;

	// integrate density change
	evolve.computeDensities(fluidModelIndex);

}

void SPH::DensityComputation_EvolveHybrid::reset()
{
	// initialize both
	evolve.reset();
	ghost.reset();

	last_sample_count = 0;
}
