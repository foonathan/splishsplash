#ifndef __Density_EvolveHybrid_h__
#define __Density_EvolveHybrid_h__

#include "Density_Ghost.h"
#include "Density_Evolve.h"

namespace SPH
{
	/** Uses density evolution for N time steps, then reset with ghost particles.
	 */
	class DensityComputation_EvolveHybrid : public DensityComputationBase
	{
	public:
		void computeDensities(unsigned int fluidModelIndex) override;

		void reset() override;

		bool clampLowDensities() const override
		{
			return false;
		}

	private:
		DensityComputation_Evolve evolve;
		DensityComputation_Ghost ghost;
		unsigned int last_sample_count;
	};
}

#endif

