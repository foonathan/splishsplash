#ifndef __DensityBase_h__
#define __DensityBase_h__

#include "SPlisHSPlasH/Common.h"
#include "SPlisHSPlasH/FluidModel.h"

namespace SPH
{
	/** \brief Interface for computing the density.
	 */
	class DensityComputationBase
	{
	public:
		virtual ~DensityComputationBase() = default;

		/** Determine densities of all fluid particles.
		*/
		virtual void computeDensities(unsigned int fluidModelIndex) = 0;

		virtual void reset() {}

		virtual bool clampLowDensities() const = 0;

	protected:
		/** The masses are slightly reduced for stability reasons.
		 *  This method resets them to 100%.
		 */
		void resetMasses(unsigned int fluidModelIndex);

		/** Whether or not the given particle is a surface particle.
		 */
		static bool isFreeSurfaceParticle(unsigned int fluidModelIndex, unsigned int i);
	};

	/** \brief Computes the density using the normal approach by sampling all neighbors.
	 */
	class DensityComputation_Standard : public DensityComputationBase
	{
	public:
		/** Computes the density of the specified particle.
		 */
		static Real computeDensityOf(FluidModel* model, int i);

		void computeDensities(unsigned int fluidModelIndex) override;

		bool clampLowDensities() const override
		{
			return true;
		}
	};
}

#endif
