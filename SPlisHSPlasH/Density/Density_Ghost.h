#ifndef __Density_Ghost_h__
#define __Density_Ghost_h__

#include <unordered_map>
#include <random>

#include "SPlisHSPlasH/TimeStep.h"

namespace SPH
{
	/** Computes densities using ghost particles.
	 */
	class DensityComputation_Ghost : public DensityComputationBase
	{
	public:
		void computeDensities(unsigned int fluidModelIndex) override;
		void reset() override;

		bool clampLowDensities() const override
		{
			return false;
		}

		void update(unsigned int fluidModelIndex);
		void resample(unsigned int fluidModelIndex);
		void integrate(unsigned int fluidModelIndex);

	private:
		typedef Eigen::Vector3i CellPos;

		struct CellPosHasher
		{
			template <class T>
			static void hash_combine(std::size_t& seed, const T& v)
			{
				std::hash<T> hasher;
				seed ^= hasher(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
			}

			std::size_t operator()(const CellPos& k) const
			{
				std::size_t result = 0;
				hash_combine(result, k[0]);
				hash_combine(result, k[1]);
				hash_combine(result, k[2]);
				return result;
			}
		};

		CellPos cell_pos_of(const Vector3r& x)
		{
			auto grid_aligned = x / cell_size;
			return grid_aligned.cast<int>();
		}

		Vector3r cell_pos_center(const CellPos& pos)
		{
			auto x = pos.x() * cell_size + cell_size / 2;
			auto y = pos.y() * cell_size + cell_size / 2;
			auto z = pos.z() * cell_size + cell_size / 2;
			return Vector3r(x, y, z);
		}

		struct BlockedCell
		{
			Vector3r position;
			int dest_index;
			unsigned depth;
		};

		void block_cell(const Vector3r& x)
		{
			blocked_cells[cell_pos_of(x)] = BlockedCell{x, -1, 0};
		}

		struct ActiveCell
		{
			Vector3r position;
			Vector3r velocity;
			unsigned depth;

			ActiveCell(const Vector3r& seed, const Vector3r& seed_velocity)
					: position(seed), depth(0) {}

			ActiveCell seed_another(const Vector3r& pos) const
			{
				ActiveCell cell(pos, velocity);
				cell.depth = depth + 1;
				return cell;
			}
		};

		void resamplePoissonDisk(unsigned int fluidModelIndex, std::vector<ActiveCell>&& seeds);

		std::default_random_engine engine;
		std::unordered_map<CellPos, BlockedCell, CellPosHasher> blocked_cells;
		Real sampling_radius, sampling_radius2, cell_size;
		Real ghost_volume;
		unsigned int last_sample_count;
	};
}

#endif
