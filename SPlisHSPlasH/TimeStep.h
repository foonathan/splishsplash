#ifndef __TimeStep_h__
#define __TimeStep_h__

#include "Common.h"
#include "ParameterObject.h"
#include "FluidModel.h"
#include "Density/DensityBase.h"

namespace SPH
{
	/** \brief Base class for the simulation methods.
	*/
	class TimeStep : public GenParam::ParameterObject
	{
	public:
		static int SOLVER_ITERATIONS;
		static int MAX_ITERATIONS;
		static int MAX_ERROR;

	protected:
		unsigned int m_iterations;
		Real m_maxError;
		unsigned int m_maxIterations;
		std::unique_ptr<DensityComputationBase> m_densityComputation;

		/** Clear accelerations and add gravitation.
		*/
		void clearAccelerations(const unsigned int fluidModelIndex);

		/** Determine densities of all fluid particles.
		*/
		void computeDensities(const unsigned int fluidModelIndex)
		{
			m_densityComputation->computeDensities(fluidModelIndex);
		}

		/** Whether or not low densities (or pressures) have to be clamped.
		 */
		bool clampLowDensities()
		{
			return m_densityComputation->clampLowDensities();
		}

		virtual void initParameters();

	public:
		TimeStep();
		virtual ~TimeStep();

		void setDensityComputation(DensityComputationBase* computation)
		{
			m_densityComputation.reset(computation);
		}

		virtual void step() = 0;
		virtual void reset();

		virtual void init();
		virtual void resize() = 0;

		virtual void emittedParticles(FluidModel *model, const unsigned int startIndex) {};
	};
}

#endif
