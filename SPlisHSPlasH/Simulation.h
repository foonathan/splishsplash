#ifndef __Simulation_h__
#define __Simulation_h__

#include "Common.h"
#include "FluidModel.h"
#include "GhostModel.h"
#include "NonPressureForceBase.h"
#include "ParameterObject.h"
#include "CompactNSearch.h"
#include "BoundaryModel.h"


/** Loop over the fluid neighbors of all fluid phases.
* Simulation *sim and FluidModel* model must be defined.
*/
#define forall_fluid_neighbors(code) \
{ \
    auto modelPID = model->getPointSetIndex(); \
    unsigned int nFluids  = sim->numberOfFluidModels(); \
    for (unsigned int id = 0; id < nFluids; id++) \
    { \
        FluidModel *fm_neighbor = sim->getFluidModel(id); \
        for (unsigned int j = 0; j < sim->numberOfNeighbors(modelPID, fm_neighbor->getPointSetIndex(), i); j++) \
        { \
            const unsigned int neighborIndex = sim->getNeighbor(modelPID, fm_neighbor->getPointSetIndex(), i, j); \
            const Vector3r &xj = fm_neighbor->getPosition(neighborIndex); \
            code \
        } \
    } \
}

/** Loop over the fluid neighbors of the same fluid phase.
* Simulation *sim, FluidModel* model must be defined.
*/
#define forall_fluid_neighbors_in_same_phase(code) \
{ \
    auto modelPID = model->getPointSetIndex(); \
    for (unsigned int j = 0; j < sim->numberOfNeighbors(modelPID, modelPID, i); j++) \
    { \
        const unsigned int neighborIndex = sim->getNeighbor(modelPID, modelPID, i, j); \
        const Vector3r &xj = model->getPosition(neighborIndex); \
        code \
    } \
}

/** Loop over the boundary neighbors of all fluid phases.
* Simulation *sim and FluidModel* model must be defined.
*/
#define forall_boundary_neighbors(code) \
{ \
    auto modelPID = model->getPointSetIndex(); \
    for (unsigned int id = 0; id < sim->numberOfBoundaryModels(); id++) \
    { \
        BoundaryModel *bm_neighbor = sim->getBoundaryModel(id); \
        for (unsigned int j = 0; j < sim->numberOfNeighbors(modelPID, bm_neighbor->getPointSetIndex(), i); j++) \
        { \
            const unsigned int neighborIndex = sim->getNeighbor(modelPID, bm_neighbor->getPointSetIndex(), i, j); \
            const Vector3r &xj = bm_neighbor->getPosition(neighborIndex); \
            code \
        } \
    } \
}

/** Loop over the ghost neighbors of all fluid phases.
* Simulation *sim and FluidModel* model must be defined.
*/
#define forall_ghost_neighbors(code) \
{ \
    auto modelPID = model->getPointSetIndex(); \
    for (unsigned int id = 0; id < sim->numberOfGhostModels(); id++) \
    { \
        GhostModel *gm_neighbor = sim->getGhostModel(id); \
        for (unsigned int j = 0; j < sim->numberOfNeighbors(modelPID, gm_neighbor->getPointSetIndex(), i); j++) \
        { \
            const unsigned int neighborIndex = sim->getNeighbor(modelPID, gm_neighbor->getPointSetIndex(), i, j); \
            const Vector3r &xj = gm_neighbor->getPosition(neighborIndex); \
            code \
        } \
    } \
}

namespace SPH
{
	enum class SimulationMethods { WCSPH = 0, PCISPH, PBF, IISPH, DFSPH, PF, NumSimulationMethods };

	enum class DensityComputationMethods { Standard = 0, Evolve, EvolveSurface, EvolveHybrid, Ghost, NumDensitycomputationMethods };

	/** \brief Class to manage the current simulation time and the time step size.
	* This class is a singleton.
	*/
	class Simulation : public GenParam::ParameterObject
	{
	public:
		static int SIM_2D;
		static int PARTICLE_RADIUS;
		static int GRAVITATION;
		static int CFL_METHOD;
		static int CFL_FACTOR;
		static int CFL_MAX_TIMESTEPSIZE;

		static int KERNEL_METHOD;
		static int GRAD_KERNEL_METHOD;
		static int ENUM_KERNEL_CUBIC;
		static int ENUM_KERNEL_WENDLANDQUINTICC2;
		static int ENUM_KERNEL_POLY6;
		static int ENUM_KERNEL_SPIKY;
		static int ENUM_KERNEL_PRECOMPUTED_CUBIC;
		static int ENUM_KERNEL_CUBIC_2D;
		static int ENUM_KERNEL_WENDLANDQUINTICC2_2D;
		static int ENUM_GRADKERNEL_CUBIC;
		static int ENUM_GRADKERNEL_WENDLANDQUINTICC2;
		static int ENUM_GRADKERNEL_POLY6;
		static int ENUM_GRADKERNEL_SPIKY;
		static int ENUM_GRADKERNEL_PRECOMPUTED_CUBIC;
		static int ENUM_GRADKERNEL_CUBIC_2D;
		static int ENUM_GRADKERNEL_WENDLANDQUINTICC2_2D;

		static int SIMULATION_METHOD;
		static int DENSITY_COMPUTATION_METHOD;

		static int ENUM_CFL_NONE;
		static int ENUM_CFL_STANDARD;
		static int ENUM_CFL_ITER;

		static int ENUM_SIMULATION_WCSPH;
		static int ENUM_SIMULATION_PCISPH;
		static int ENUM_SIMULATION_PBF;
		static int ENUM_SIMULATION_IISPH;
		static int ENUM_SIMULATION_DFSPH;
		static int ENUM_SIMULATION_PF;

		static int ENUM_DENSITY_STANDARD;
		static int ENUM_DENSITY_EVOLVE;
		static int ENUM_DENSITY_EVOLVE_SURFACE;
		static int ENUM_DENSITY_EVOLVE_HYBRID;
		static int ENUM_DENSITY_GHOST;

		typedef PrecomputedKernel<CubicKernel, 10000> PrecomputedCubicKernel;

	protected:
		std::vector<FluidModel*> m_fluidModels;
		std::vector<BoundaryModel*> m_boundaryModels;
        std::vector<GhostModel*> m_ghostModels;
		CompactNSearch::NeighborhoodSearch *m_neighborhoodSearch;
		int m_cflMethod;
		Real m_cflFactor;
		Real m_cflMaxTimeStepSize;
		int m_kernelMethod;
		int m_gradKernelMethod;
		Real m_W_zero;
		Real(*m_kernelFct)(const Vector3r &);
		Vector3r(*m_gradKernelFct)(const Vector3r &r);
		SimulationMethods m_simulationMethod;
		DensityComputationMethods m_densityComputationMethod;
		TimeStep *m_timeStep;
		Vector3r m_gravitation;
		Real m_particleRadius;
		Real m_supportRadius;
		bool m_sim2D;
		std::function<void()> m_simulationMethodChanged;

		virtual void initParameters();

	private:
		static Simulation *current;

	public:
		Simulation ();
		~Simulation ();

		void init(const Real particleRadius, const bool sim2D);
		void reset();

		// Singleton
		static Simulation* getCurrent ();
		static void setCurrent (Simulation* tm);
		static bool hasCurrent();

		void addFluidModel(const std::string &id, const unsigned int nFluidParticles, Vector3r* fluidParticles, Vector3r* fluidVelocities, const unsigned int nMaxEmitterParticles);
		FluidModel *getFluidModel(unsigned int index) { return m_fluidModels[index]; }
		PointSetIndex getFluidModelPID(unsigned int index) { return m_fluidModels[index]->getPointSetIndex(); }
		unsigned int numberOfFluidModels() const { return static_cast<unsigned int>(m_fluidModels.size()); }

		void addBoundaryModel(RigidBodyObject *rbo, const unsigned int numBoundaryParticles, Vector3r *boundaryParticles);
		BoundaryModel *getBoundaryModel(unsigned int index) { return m_boundaryModels[index]; }
        PointSetIndex getBoundaryModelPID(unsigned int index) { return m_boundaryModels[index]->getPointSetIndex(); }
		unsigned int numberOfBoundaryModels() const { return static_cast<unsigned int>(m_boundaryModels.size()); }
		void updateBoundaryVolume();

		// there is one ghost model for each fluid model
		GhostModel* getGhostModel(const unsigned int index) { return m_ghostModels[index]; }
		unsigned int numberOfGhostModels() const { return static_cast<unsigned int>(m_ghostModels.size()); }
        PointSetIndex getGhostModelPID(unsigned int index) { return m_ghostModels[index]->getPointSetIndex(); }

		int getKernel() const { return m_kernelMethod; }
		void setKernel(int val);
		int getGradKernel() const { return m_gradKernelMethod; }
		void setGradKernel(int val);

		FORCE_INLINE Real W_zero() const { return m_W_zero; }
		FORCE_INLINE Real W(const Vector3r &r) const { return m_kernelFct(r); }
		FORCE_INLINE Vector3r gradW(const Vector3r &r) { return m_gradKernelFct(r); }

		int getSimulationMethod() const { return static_cast<int>(m_simulationMethod); }
		void setSimulationMethod(const int val);

		int getDensityComputationMethod() const { return static_cast<int>(m_densityComputationMethod); }
		void setDensityComputationMethod(const int val);

		void setSimulationMethodChangedCallback(std::function<void()> const& callBackFct);

		TimeStep *getTimeStep() { return m_timeStep; }

		bool is2DSimulation() { return m_sim2D; }

		void setParticleRadius(Real val);
		Real getParticleRadius() const { return m_particleRadius; }
		Real getSupportRadius() const { return m_supportRadius; }

		/** Update time step size depending on the chosen method.
		*/
		void updateTimeStepSize();

		/** Update time step size by CFL condition.
		*/
		void updateTimeStepSizeCFL(const Real minTimeStepSize);

		/** Perform the neighborhood search for all fluid particles.
		*/
		virtual void performNeighborhoodSearch();
		void performNeighborhoodSearchSort();

		void computeNonPressureForces();

		void emitParticles();
		virtual void emittedParticles(FluidModel *model, const unsigned int startIndex);

		CompactNSearch::NeighborhoodSearch* getNeighborhoodSearch() { return m_neighborhoodSearch; }

		FORCE_INLINE unsigned int numberOfPointSets() const
		{
			return static_cast<unsigned int>(m_neighborhoodSearch->n_point_sets());
		}

		FORCE_INLINE unsigned int numberOfNeighbors(PointSetIndex pointSetIndex, PointSetIndex neighborPointSetIndex, const unsigned int index) const
		{
			return static_cast<unsigned int>(m_neighborhoodSearch->point_set(pointSetIndex.id).n_neighbors(neighborPointSetIndex.id, index));
		}

		FORCE_INLINE unsigned int getNeighbor(PointSetIndex pointSetIndex, PointSetIndex neighborPointSetIndex, const unsigned int index, const unsigned int k) const
		{
			return m_neighborhoodSearch->point_set(pointSetIndex.id).neighbor(neighborPointSetIndex.id, index, k);
		}
	};
}

#endif
