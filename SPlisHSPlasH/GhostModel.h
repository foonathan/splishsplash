#ifndef __GhostModel_h__
#define __GhostModel_h__

#include "Common.h"
#include <vector>

namespace SPH
{
    /** \brief The ghost model stores ghost particles for density computation.
     */
    class GhostModel
    {
        std::vector<Vector3r> m_x;
        std::vector<Vector3r> m_v;
        unsigned int m_pointSetIndex;

    public:
        void init();

        void reset();

        void resize(const unsigned int nGhostParticles, Vector3r* ghostParticles, Vector3r* ghostVelocities);

        unsigned int numberOfParticles() const { return static_cast<unsigned int>(m_x.size()); }

        PointSetIndex getPointSetIndex() const { return {m_pointSetIndex}; }

        FORCE_INLINE Vector3r &getPosition(const unsigned int i)
        {
            return m_x[i];
        }

        FORCE_INLINE const Vector3r &getPosition(const unsigned int i) const
        {
            return m_x[i];
        }

        FORCE_INLINE void setPosition(const unsigned int i, const Vector3r &pos)
        {
            m_x[i] = pos;
        }

        FORCE_INLINE Vector3r &getVelocity(const unsigned int i)
        {
            return m_v[i];
        }

        FORCE_INLINE const Vector3r &getVelocity(const unsigned int i) const
        {
            return m_v[i];
        }

        FORCE_INLINE void setVelocity(const unsigned int i, const Vector3r &vel)
        {
            m_v[i] = vel;
        }
    };
} // namespace SPH

#endif
