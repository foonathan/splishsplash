#include "GhostModel.h"
#include "Simulation.h"

using namespace SPH;

void GhostModel::init()
{
    CompactNSearch::NeighborhoodSearch *neighborhoodSearch = Simulation::getCurrent()->getNeighborhoodSearch();
    m_pointSetIndex = neighborhoodSearch->add_point_set(nullptr, 0, true, true, true, this);
}

void GhostModel::resize(const unsigned int nGhostParticles, Vector3r *ghostParticles, Vector3r *ghostVelocities)
{
    m_x.resize(nGhostParticles);
    m_v.resize(nGhostParticles);

#pragma omp parallel default(shared)
    {
#pragma omp for schedule(static)
        for (int i = 0; i < (int) nGhostParticles; i++)
        {
            m_x[i] = ghostParticles[i];
            m_v[i] = ghostVelocities[i];
        }
    }

    CompactNSearch::NeighborhoodSearch *neighborhoodSearch = Simulation::getCurrent()->getNeighborhoodSearch();
    neighborhoodSearch->resize_point_set(m_pointSetIndex, &m_x[0][0], m_x.size());
}

void GhostModel::reset()
{
    m_x.clear();
    m_v.clear();

    CompactNSearch::NeighborhoodSearch *neighborhoodSearch = Simulation::getCurrent()->getNeighborhoodSearch();
    neighborhoodSearch->resize_point_set(m_pointSetIndex, nullptr, 0);
}

